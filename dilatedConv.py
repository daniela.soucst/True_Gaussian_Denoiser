import torch
import torch.nn as nn

class DilatedConv2dBlock(nn.Module):
    # This module keeps the input feature maps dimensions!!!
    def __init__(self, inputs, outputs, padding=1, bias=False):
        super(DilatedConv2dBlock, self).__init__()
        self.conv1 = nn.Conv2d(inputs, outputs, kernel_size=3, stride=1, padding=padding, dilation=1, bias=bias)
        self.conv2 = nn.Conv2d(inputs, outputs, kernel_size=3, stride=1, padding=(padding + 1 * padding), dilation=2, bias=bias)
        self.conv3 = nn.Conv2d(inputs, outputs, kernel_size=3, stride=1, padding=(padding + 2 * padding), dilation=3, bias=bias)
        self.conv = nn.Conv2d((3 * outputs), outputs, 3, stride=1, padding=padding, bias=bias)

    def forward(self, inputs):
        outputs1 = self.conv1(inputs)
        outputs2 = self.conv2(inputs)
        outputs3 = self.conv3(inputs)
        outputs = torch.cat((outputs1, outputs2, outputs3), dim=1)
        outputs = self.conv(outputs)

        return outputs

